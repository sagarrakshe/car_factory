Car Factory
===========

Sample demo of factory design pattern (Python3).

Usage:

    $ python build_car.py -L <layout>

Example:

    $ python build_car.py -L Sedan

Check logs in `logs/test.log`

Tests:

    $ python test.py

