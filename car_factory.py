import logging

from sedan import sedan_conf
from saloon import saloon_conf
from car_registry import CarRegistry

logger = logging.getLogger(__name__)

def get_car_factory(layout="Sedan"):
    """The factory method"""

    logger.info('Checking layout %s ', layout)
    if not CarRegistry.AVAILABLE_LAYOUTS.get(layout, None):
        logger.error('%s layout does not exists', layout)
        raise RuntimeError('"{}" layout does not exists'.format(layout))

    return CarRegistry.AVAILABLE_LAYOUTS[layout]()
