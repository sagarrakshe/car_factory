import os
import logging
import argparse

from car_factory import get_car_factory

logger = logging.getLogger()
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
handler = logging.FileHandler('{}/logs/test.log'.format(os.getcwd()))

logger.addHandler(handler)
logger.setLevel(logging.DEBUG)
handler.setFormatter(formatter)

def main(layout):
    car = get_car_factory(layout)
    return car.build_and_race()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create car of given type')
    parser.add_argument('--layout', '-L',  type=str, help='Enter car layout')

    args = parser.parse_args()

    if not args.layout:
        parser.print_help()
    else:
        main(args.layout)
