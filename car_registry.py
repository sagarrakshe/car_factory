class _LayoutRegistry(type):
    """
    MetaClass for car Layouts.
    """

    AVAILABLE_LAYOUTS = {}

    def __new__(cls, *args, **kwargs):

        class_name = args[0]
        base_classes = args[1]
        attributes = args[2]

        cls.AVAILABLE_LAYOUTS[class_name] = super(_LayoutRegistry,
                cls).__new__(cls, *args, **kwargs)

        return super(_LayoutRegistry, cls).__new__(cls, *args, **kwargs)

class CarRegistry(_LayoutRegistry):
    pass
