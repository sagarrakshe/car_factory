import logging

from car.base import BaseCar
from car_registry import _LayoutRegistry

logger = logging.getLogger(__name__)

class Sedan(BaseCar, metaclass=_LayoutRegistry):
    def build_and_race(self):
        logger.info('Building {name}...'.format(name=self.__class__.__name__))
        return True
