class BaseCar(object):
    """
    Base Car Class.
    """

    @property
    def build_and_race(self):
        raise NotImplementedError
