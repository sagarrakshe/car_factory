import logging
import unittest

from build_car import main
from car.base import BaseCar
from car_registry import _LayoutRegistry

logger = logging.getLogger(__name__)

class TestMakeCar(unittest.TestCase):
    def test_dynamic_car(self):
        class Tesla(BaseCar, metaclass=_LayoutRegistry):
            def build_and_race(self):
                logger.info('Building {name}...'.format(
                    name=self.__class__.__name__))
                return True

        res = main('Tesla')
        self.assertEqual(res, True)


    def test_exists_layout_car(self):
        res = main('Saloon')
        self.assertEqual(res, True)


    def test_does_not_exists_layout_car(self):
        _type = 'Limo'
        with self.assertRaisesRegex(RuntimeError,
                '"{}" layout does not exists'.format(_type)):
                main(_type)


if __name__ == '__main__':
    unittest.main()
